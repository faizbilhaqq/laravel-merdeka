<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <!-- <link href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet"> -->

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css">
     
    <!-- <link rel="stylesheet" href="public/style.css"> -->
    <!-- <script src="script.js"></script> -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;900&family=Ubuntu:wght@700&display=swap" rel="stylesheet">


        <!-- Styles -->

        <style>
          body{
    font-family: 'Montserrat', sans-serif;
font-family: 'Ubuntu', sans-serif;
}

#hero-picture{
    height: 500px;
    width: 350px;
}

#project img{
    width: 350px;
    height: 250px;
}

#techstack h1{
    text-align: center;
    margin-top: 80px;
    margin-bottom: 80px;
}

.foto1{
    width: 200px;
    height: 200px;
    margin-top: 40px;
}

#footer{
    margin-top: 80px;
}
        </style>

    </head>
    <body >
    <section id="header">

<div class="container">
    <header class="d-flex flex-wrap justify-content-center py-3 mb-4 border-bottom">
      <a href="/" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto text-dark text-decoration-none">
        <i class="fa-solid fa-laptop-code fa-4x"></i>
        <span class="fs-4">My Personal Homepage</span>
      </a>

      <ul class="nav ">
        <li class="nav-item"><a href="#content" class="nav-link active" aria-current="page">Intro</a></li>
        <li class="nav-item"><a href="#project" class="nav-link">Project</a></li>
        <li class="nav-item"><a href="#techstack" class="nav-link">My tech-stack</a></li>

    </header>
  </div>


</section>

<section id="content" >
<div class="konten">
    <div class="container col-xxl-8 px-4 py-5">
        <div class="row flex-lg-row-reverse align-items-center g-5 py-5">
          <div class="col-10 col-sm-8 col-lg-6">
            <img src="/projek2an/resources/media/WhatsApp Image 2022-09-07 at 13.16.33.jpeg" id="hero-picture" class=" d-block mx-lg-auto img-fluid"  alt="Bootstrap Themes"  loading="lazy">
            <p>click me</p>
          </div>
          <div class="col-lg-6">
            <h1 class="display-5 fw-bold lh-1 mb-3">Hello, Let Me Introduce Myself!</h1>
            <p class="lead">Hello there, my name is Muhammad Faiz Bilhaqq, but you can call me Faiz or Bilhaqq (my student's mate call me Bilhaqq 'cause there are two faiz there haha). I'm final year student of SBM ITB. My journey to becoming developer start when i learn python which my first programming language and i'm very curious, enjoying, and wanna learn more, then here we are. Currently, i'm fullstack engineer intern at Investree. Cannot wait to learn more here !</p>
            <div class="d-grid gap-2 d-md-flex justify-content-md-start">
              <button type="button" id="sayhello" class="btn btn-primary btn-lg px-4 me-md-2">Say Hello</button>
              <!-- <button type="button" class="btn btn-outline-secondary btn-lg px-4">Default</button> -->
            </div>
          </div>
        </div>
      </div>
</div>
</section>

<section id="project" >
<div class="row" >
<div class="col-lg-4 " >
<div class="card" >
<img src="/projek2an/resources/media/webscrapper.png" class="card-img-top" alt="...">
<div class="card-body">
<h5 class="card-title">YC-Hacker News Web Scrapper</h5>
<p class="card-text">Use Axios to fetch news data from YC-Hacker News to my website that contain title, headline, and link to the website </p>
<!-- <a href="#" class="btn btn-primary">Go somewhere</a> -->
</div>
</div>

</div>

<div class="col-lg-4" >
<div class="card" >
<img src="/projek2an/resources/media/weatherApp.png" class="card-img-top" alt="...">
<div class="card-body">
<h5 class="card-title">Weather App</h5>
<p class="card-text">Use fetch to get data from Weather API and then display it</p>
<!-- <a href="#" class="btn btn-primary">Go somewhere</a> -->
</div>
</div>

</div>

<div class="col-lg-4" >
<div class="card" >
<img src="/projek2an/resources/media/covidTracker.png" class="card-img-top" alt="...">
<div class="card-body">
<h5 class="card-title">Covid Tracker</h5>
<p class="card-text">Use fetch to get data from Covid API and then display it</p>
<!-- <a href="#" class="btn btn-primary">Go somewhere</a> -->
</div>
</div>

</div>
</div>


</section>

<section id="techstack" >

<h1>My Tech Stack</h1>

<div class="container text-center">
<div class="row" >
<div class="col-lg-3" ><img class="foto1" src="https://assets.stickpng.com/images/5848152fcef1014c0b5e4967.png" alt=""></div>
<div class="col-lg-3" ><img class="foto1" src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/b2/Bootstrap_logo.svg/2560px-Bootstrap_logo.svg.png" alt=""></div>
<div class="col-lg-3"><img class="foto1" src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Unofficial_JavaScript_logo_2.svg/2048px-Unofficial_JavaScript_logo_2.svg.png" alt=""></div>
<div class="col-lg-3"><img class="foto1" src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/Node.js_logo.svg/590px-Node.js_logo.svg.png" alt=""></div>
<div class="col-lg-3"><img class="foto1" src="https://cdn.icon-icons.com/icons2/2699/PNG/512/expressjs_logo_icon_169185.png" alt=""></div>
<div class="col-lg-3"><img class="foto1" src="https://cdn1.iconfinder.com/data/icons/programing-development-8/24/react_logo-512.png" alt=""></div>
<div class="col-lg-3"><img class="foto1" src="https://upload.wikimedia.org/wikipedia/commons/f/f1/Vue.png" alt=""></div>
<div class="col-lg-3"><img class="foto1" src="https://www.php.net/images/logos/new-php-logo.svg" alt=""></div>
<div class="col-lg-3"><img class="foto1" src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Laravel.svg/985px-Laravel.svg.png" alt=""></div>
<div class="col-lg-3"><img class="foto1" src="https://cdn.icon-icons.com/icons2/2415/PNG/512/mongodb_original_logo_icon_146424.png" alt=""></div>
<div class="col-lg-3"><img class="foto1" src="https://camo.githubusercontent.com/7c669e872b214571ae0b5097e8d3db369225a806dc2ce9a436cde3497164310c/687474703a2f2f6d6f6e676f64622d746f6f6c732e636f6d2f696d672f6d6f6e676f6f73652e706e67" alt=""></div>
<div class="col-lg-3"><img class="foto1" src="https://download.logo.wine/logo/MySQL/MySQL-Logo.wine.png" alt=""></div>

</section>

<section id="footer" >
<div class="container ">
    <footer class="d-flex flex-wrap justify-content-between align-items-center py-3 my-4 border-top">
      <div class="col-md-4 d-flex align-items-center">
        <a href="/" class="mb-3 me-2 mb-md-0 text-muted text-decoration-none lh-1">
          <svg class="bi" width="30" height="24"><use xlink:href="#bootstrap"/></svg>
        </a>
        <span class="mb-3 mb-md-0 text-muted">My Personal HomePage</span>
      </div>
  
      <ul class="nav col-md-4 justify-content-end list-unstyled d-flex">
        <li class="ms-3"><a class="text-muted" href="#"><i class="fa-brands fa-twitter fa-2x" ></i></a></li>
        <li class="ms-3"><a class="text-muted" href="#"><i class="fa-brands fa-instagram fa-2x " ></i></a></li>
        <li class="ms-3"><a class="text-muted" href="#"><i class="fa-brands fa-facebook fa-2x " ></i></a></li>
      </ul>
    </footer>
  </div>
  

</section>   

<script>
document.getElementById("hero-picture").addEventListener("click", function() {
  document.getElementById("hero-picture").src= "/projek2an/resources/media/faizitb.jpeg" ;
});

document.getElementById("sayhello").addEventListener("click", function() {
  alert("hello over there!!!") ;
});

</script>
    </body>
</html>
